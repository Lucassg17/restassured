package steps;


import constantes.AtributosJson;
import constantes.EndPoints;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.restassured.response.Response;
import metodosTestes.Metodos;
import org.junit.Assert;

public class StepTheCatApi {
    Response votar;
    Response retornaImagem;
    Response retornaImagens;
    Response votoCriado;

    ////////////////////@Teste01 @%TheCatApi

    @Dado("que eu informe a url {string} que retorna uma imagem especifica")
    public void queEuInformeAUrlQueRetornaUmaImagemEspecifica(String url){
        retornaImagem = Metodos.retornaImagem(url);
    }

    @Entao("ao processar confirmo que retornou apenas uma imagem")
    public void aoProcessarConfirmoQueRetornouApenasUmaImagem() {
        Assert.assertEquals(retornaImagem.statusCode(), 200);
        Assert.assertEquals(retornaImagem.jsonPath().getString(AtributosJson.url), EndPoints.urlImagemEspecifica);
    }

    ////////////////////@Teste02 @%TheCatApi

    @Dado("que eu informe a url {string} que retorna dez imagens")
    public void queEuInformeAUrlQueRetornaDezImagens(String url) {
        retornaImagens = Metodos.retornaMaisDeUmaImagem(url);
    }

    @Entao("apos processar confirmo que o tamnho do meu array foi dez")
    public void aposProcessarConfirmoQueOTamnhoDoMeuArrayFoiDez() {
        Assert.assertEquals(retornaImagens.jsonPath().getInt(AtributosJson.size), EndPoints.tamanhoDoArray);
    }

    ////////////////////@Teste03 @%TheCatApi

    @Dado("que eu infome a url {string} que realiza um voto em uma imagem")
    public void queEuInfomeAUrlQueRealizaUmVotoEmUmaImagem(String urlVoto) {
        votar = Metodos.gerarVoto(urlVoto);
    }

    @Entao("ao processar confirmo que meu voto foi realizado")
    public void aoProcessarConfirmoQueMeuVotoFoiRealizado() {
        Assert.assertEquals(votar.jsonPath().getString(AtributosJson.message), "SUCCESS");
    }

    ////////////////////@Teste04 @%TheCatApi

    @Dado("que eu infome a url {string} para deletar um voto")
    public void queEuInfomeAUrlParaDeletarUmVoto(String votoDeletado) {
        votoCriado = Metodos.deveDeletarUmVotoQuandoSolicitado(votoDeletado);
    }

    @Entao("apos processar confirmo que foi gerado um voto e depois o mesmo foi deletado")
    public void aposProcessarConfirmoQueFoiGeradoUmVotoEDepoisOMesmoFoiDeletado() {
        Assert.assertEquals(votoCriado.jsonPath().getString(AtributosJson.message), "SUCCESS");
    }
}
