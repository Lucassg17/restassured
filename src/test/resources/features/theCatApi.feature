#language: pt

Funcionalidade: Testar metodos HTTP do The Cat API

  @Teste01 @%TheCatAPI
  Cenario: Retornar uma Imagem Especifica
    Dado que eu informe a url "https://api.thecatapi.com/v1/images/9ab" que retorna uma imagem especifica
    Entao ao processar confirmo que retornou apenas uma imagem

  @Teste02 @%TheCatApi
  Cenario: Retorna 10 imagens
    Dado que eu informe a url "https://api.thecatapi.com/v1/images/search" que retorna dez imagens
    Entao apos processar confirmo que o tamnho do meu array foi dez

  @Teste03 @%TheCatApi
  Cenario: Realizar um voto em uma imagem
    Dado que eu infome a url "https://api.thecatapi.com/v1/votes" que realiza um voto em uma imagem
    Entao ao processar confirmo que meu voto foi realizado

  @Teste04 @%TheCatApi
  Cenario: Deletar um voto
    Dado que eu infome a url "https://api.thecatapi.com/v1/votes" para deletar um voto
    Entao apos processar confirmo que foi gerado um voto e depois o mesmo foi deletado


