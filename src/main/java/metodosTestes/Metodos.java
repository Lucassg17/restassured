package metodosTestes;

import constantes.AtributosJson;
import constantes.EndPoints;
import io.restassured.response.Response;
import org.example.AppTest;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;

import java.util.HashMap;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class Metodos {
    public static HashMap<String, String> header = new HashMap<>();

    public static Response retornaImagem(String urlImagemEspecifica){
        Response imagem =
                given()
                .when()
                    .get(urlImagemEspecifica)
                .then()
                    .extract().response();

        return imagem;
    }

    public static Response retornaMaisDeUmaImagem(String urlMaisDeUmaImagem){
        Response imagens =
                given()
                    .param(AtributosJson.limit, 10)
                .when()
                    .get(urlMaisDeUmaImagem)
                .then()
                    .extract().response();

        return imagens;
    }

    public static Response gerarVoto(String urlParaVotar){
        header.put("x-api-key", "live_VxvJtDy6cWvQjCVkwEDK8CM9J2Y3MgFvXMaKddGxD81HcmFGDAVyrTPepwNyJSje");

        Response respons =
                given()
                    .headers(header)
                    .contentType("application/json")
                    .body(geraCorpoDoVoto())
                .when()
                    .post(urlParaVotar)
                .then()
                    .extract().response(); // extrai a resposta que eu tive após gerar um voto

        return respons;
    }

    public static Response deveDeletarUmVotoQuandoSolicitado(String urlParaDeletarUmVoto){
        header.put("x-api-key", "live_VxvJtDy6cWvQjCVkwEDK8CM9J2Y3MgFvXMaKddGxD81HcmFGDAVyrTPepwNyJSje");

        Response votoCriado = Metodos.gerarVoto(urlParaDeletarUmVoto);

        String id = votoCriado.jsonPath().getString("id");// pega, dentro do boby, o valor do campo id

        given()
            .headers(header)
        .when()
            .delete(urlParaDeletarUmVoto + "/" + id)
        .then()
            .extract().response();

        return votoCriado;
    }

    public static String geraCorpoDoVoto(){
        JSONObject voto = new JSONObject();
        voto.put(AtributosJson.imageId, "ol");
        voto.put(AtributosJson.value, 1);

        return voto.toString();
    }
}
