package constantes;

public class AtributosJson {
    public static final String message = "message";
    public static final String limit = "limit";
    public static final String url = "url";
    public static final String size = "size()";
    public static final String imageId = "image_id";
    public static final String value = "value";
}
